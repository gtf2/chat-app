cd src/main/resources/ui
export NODE_OPTIONS=--max_old_space_size=8192
npm install --registry=http://registry.npmjs.org/
npm run build

cd ../../../..

mvn clean install -DskipTests=true -Pprod
