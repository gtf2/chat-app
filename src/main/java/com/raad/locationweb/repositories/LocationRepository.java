package com.raad.locationweb.repositories;


import com.raad.locationweb.entity.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocationRepository extends JpaRepository<Location ,Integer> {

    @Query(value ="select type, count(type) from location group by type",nativeQuery = true)
    public List<Object[]>findTypeAndTypeLocation();
}
