package com.raad.locationweb.services;

import com.raad.locationweb.entity.Location;

import java.util.List;
import java.util.Optional;

public interface LocationService {

    Location addLocation(Location location);
    Location updateLocation(Location location);
    void deleteLocation(Location location);
    Optional<Location> findLocationById(int id);
    List<Location> getAllLocation();
}
