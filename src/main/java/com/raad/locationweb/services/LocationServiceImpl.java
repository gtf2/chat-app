package com.raad.locationweb.services;

import com.raad.locationweb.entity.Location;
import com.raad.locationweb.repositories.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LocationServiceImpl implements LocationService {

    @Autowired
    private LocationRepository locationRepository;

    public LocationRepository getLocationRepository() {
        return locationRepository;
    }

    public void setLocationRepository(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    @Override
    public Location addLocation(Location location) {
        return locationRepository.save(location);
    }

    @Override
    public Location updateLocation(Location location) {
        return locationRepository.save(location);
    }

    @Override
    public void deleteLocation(Location location) {
            locationRepository.delete(location);
    }

    @Override
    public Optional<Location> findLocationById(int id) {
        return locationRepository.findById(id);
    }

    @Override
    public List<Location> getAllLocation() {
        return locationRepository.findAll();
    }
}
