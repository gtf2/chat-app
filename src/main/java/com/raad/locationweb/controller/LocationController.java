package com.raad.locationweb.controller;

import com.raad.locationweb.entity.Location;
import com.raad.locationweb.repositories.LocationRepository;
import com.raad.locationweb.services.LocationService;
import com.raad.locationweb.util.EmailUtil;
import com.raad.locationweb.util.ReportUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletContext;
import java.util.List;
import java.util.Optional;

@Controller
public class LocationController {

    @Autowired
    LocationService locationService;

    @Autowired
    LocationRepository repository;

    @Autowired
    EmailUtil emailUtil;

    @Autowired
    ReportUtil reportUtil;

    @Autowired
    ServletContext servletContext;

    @RequestMapping("/showCreate")
    public String showCreate() {
        return "createLocation";
    }

    @RequestMapping("/saveLoc")
    public String saveLocation(@ModelAttribute("location") Location location, ModelMap modelMap) {
        Location locationSaved = locationService.addLocation(location);
        String msg = "Location saved with ID :" + location.getId();
        modelMap.addAttribute("msg", msg);
        emailUtil.sendMail("zamharrad@gamil.com", "Location saved", "Location saved successfuly");
        return "createLocation";
    }

    @RequestMapping("/displayLocation")
    public String displayLocation(ModelMap modelMap) {
        List<Location> locations = locationService.getAllLocation();
        modelMap.addAttribute("locations", locations);
        return "displayLocation";
    }

    @RequestMapping("/deleteLocation")
    public String deleteLocation(@RequestParam("id") int id, ModelMap modelMap) {
        Location location = new Location();
        location.setId(id);
        locationService.deleteLocation(location);
        List<Location> locations = locationService.getAllLocation();
        modelMap.addAttribute("locations", locations);
        return "displayLocation";
    }

    @RequestMapping("/showUpdate")
    public String showUpdate(@RequestParam("id") int id, ModelMap modelMap) {
        Optional<Location> location = locationService.findLocationById(id);
        if (location.isPresent()) {
            modelMap.addAttribute("location", location.get());
        } else {
            // ERROR?
        }
        modelMap.addAttribute("location", location.get());
        return "updateLocation";
    }

    @RequestMapping("/updateLoc")
    public String updateLocation(@ModelAttribute("location") Location location, ModelMap modelMap) {
        locationService.updateLocation(location);
        List<Location> locations = locationService.getAllLocation();
        modelMap.addAttribute("locations", locations);
        return "displayLocation";
    }

    @RequestMapping("/generateReport")
    public String generateReport() {
        String path = servletContext.getRealPath("/");
        List<Object[]> data = repository.findTypeAndTypeLocation();
        reportUtil.genaratePieChart(path, data);
        return "report";
    }
}
