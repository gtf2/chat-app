package com.raad.locationweb.controller;

import com.raad.locationweb.entity.Location;
import com.raad.locationweb.repositories.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/locations")
public class LocationRESTController {

    @Autowired
    LocationRepository locationRepository;

    @GetMapping
    public List<Location> getLocations(){
        return locationRepository.findAll();
    }

    @PostMapping()
    public Location createLocation(@RequestBody Location location){
        return locationRepository.save(location);

    }

    @PutMapping()
    public Location updateLocation(@RequestBody Location location){
        return locationRepository.save(location);
    }

    @DeleteMapping("/{id}")
    public void deleteLocation(@PathVariable("id") int id){
        locationRepository.deleteById(id);
    }

    @GetMapping("/{id}")
    public Location findLocationById(@PathVariable("id") int id){

        Optional<Location> location= locationRepository.findById(id);
        if(location.isPresent()){
            return location.get();
        }else {

        }
        return location.get();
    }
}
