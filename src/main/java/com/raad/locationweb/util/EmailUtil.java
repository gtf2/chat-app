package com.raad.locationweb.util;

public interface EmailUtil {

    void sendMail(String toAddress, String subject, String body);
}
