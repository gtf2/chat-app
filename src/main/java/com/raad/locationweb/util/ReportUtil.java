package com.raad.locationweb.util;

import javax.persistence.Entity;
import java.util.List;


public interface ReportUtil {
     void genaratePieChart(String path, List<Object[]> data);
}

