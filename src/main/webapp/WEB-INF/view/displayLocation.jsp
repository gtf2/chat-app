<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page isELIgnored="false" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Display Location</title>
</head>
<body>
<h2>Display Location</h2>
<table>
    <tr>
        <td>ID</td>
        <td>CODE</td>
        <td>NAME</td>
        <td>TYPE</td>
    </tr>
    <c:forEach items="${locations}" var="location">
        <tr>
            <td>${location.id}</td>
            <td>${location.code}</td>
            <td>${location.name}</td>
            <td>${location.type}</td>
            <td><a href="deleteLocation?id=${location.id}">Delete</a></td>
            <td><a href="showUpdate?id=${location.id}">Edit</a></td>
        </tr>
    </c:forEach>
</table>

<a href="showCreate">Add new location</a>
</body>
</html>