<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page isELIgnored="false" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Create Location</title>
</head>
<body>
<form action="updateLoc" method="post" />
<pre>
    ID: <input type="text" name="id" value="${location.id}" readonly />  <br>
    CODE: <input type="text" name="code" value="${location.code}" /><br>
    NAME: <input type="text" name="name" value="${location.name}" /><br>
    TYPE:   URBAN <input type="radio" value="URBAN" name="type" ${location.type=='URBAN'?'checked':''} />
            RURAL <input type="radio" value="RURAL" name="type" ${location.type=='RURAL'?'checked':''} />
    
    <input type="submit" value="SAVE" />
</pre>
    </form>

</body>
</html>